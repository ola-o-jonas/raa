// ==UserScript==
// @name     Raa
// @author Sweden Reviewers
// @version  1.5
// @include        http://www.geocaching.com/geocache/*
// @include        https://www.geocaching.com/geocache/*
// @include        http://www.geocaching.com/admin/*
// @include        https://www.geocaching.com/admin/*
// @icon https://i.imgur.com/zA6Y7hV.png
// @updateURL https://gitlab.com/ola-o-jonas/raa/-/raw/master/Raa.user.js
// @downloadURL https://gitlab.com/ola-o-jonas/raa/-/raw/master/Raa.user.js
// @grant       GM.xmlHttpRequest
// @connect     kulturarvsdata.se
// @run-at      document-idle
// ==/UserScript==

/*
 * Program logic
 * -------------
 * 1. Find all instances of coordinates.
 * 2. For each instance:
 *      1. Create the Raa-icon-a-tag.
 *      2. set data-coordinates.
 *      3. Insert into DOM where suitable.
 * 3. Loop each raa-instance and fetch from Fornsök api:
 *      1. If it found anything for the coordinate, flag it.
 *      2. If it didn't, mark it green.
 * 4. Clicking the Raa-icon will open https://app.raa.se/open/fornsok/plats-query?n=XXX&e=YYY
 *      (where XXX and YYY are Sweref 99TM formatted coordinates)
 * 5. Separate greasemonkey script will update inputs and search the area at the Fornsök map.
 */

//This is just hte svg-icon for Raa
let raaSvg = '<?xml version="1.0" encoding="UTF-8"?><svg version="1.2" baseProfile="tiny" id="Lager_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="5 0 110 100" xml:space="preserve"><path fill="" d="M85.3,56.2H56.6v7h28.9c5.5,0.2,10,4.7,10,10.3c0,5.7-4.6,10.3-10.3,10.3c-5.6,0-10.1-4.4-10.3-10v-6.3h-7  v6.2c0,9.7,7.9,17.5,17.6,17.5S103,83.3,103,73.7C102.9,64,95,56.2,85.3,56.2 M45.3,56.2v17.7c-0.2,5.5-4.7,10-10.3,10  c-5.7,0-10.3-4.6-10.3-10.3c0-5.6,4.4-10.1,10-10.3H41v-7h-6.2c-9.7,0-17.5,7.9-17.5,17.6s7.9,17.5,17.5,17.5  c9.7,0,17.6-7.9,17.6-17.5V45h-7v11.2L45.3,56.2z M34.8,40.7h28.7v-7H34.7c-5.5-0.2-10-4.7-10-10.3c0-5.7,4.6-10.3,10.3-10.3  c5.6,0,10.1,4.4,10.3,10v6.3h7v-6.2c0-9.7-7.9-17.5-17.5-17.5c-9.7,0-17.5,7.9-17.5,17.5C17.2,32.9,25.1,40.7,34.8,40.7 M74.8,40.7  V23.1c0.2-5.5,4.7-10,10.3-10c5.7,0,10.3,4.6,10.3,10.3c0,5.6-4.4,10.1-10,10.3h-6.3v7h6.2c9.7,0,17.5-7.9,17.5-17.5  c0-9.7-7.9-17.6-17.5-17.6c-9.7,0-17.5,7.9-17.5,17.6v28.7h7V40.7z"/></svg>';
let reloadSvg = '<?xml version="1.0" encoding="UTF-8" standalone="no"?><svg xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:cc="http://creativecommons.org/ns#" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#" xmlns:svg="http://www.w3.org/2000/svg" xmlns="http://www.w3.org/2000/svg" xmlns:sodipodi="http://sodipodi.sourceforge.net/DTD/sodipodi-0.dtd" xmlns:inkscape="http://www.inkscape.org/namespaces/inkscape" viewBox="0 -256 1792 1792" id="svg2" version="1.1" inkscape:version="0.48.3.1 r9886" width="100%" height="100%" sodipodi:docname="refresh_font_awesome.svg"><metadata id="metadata12"><rdf:RDF><cc:Work rdf:about=""><dc:format>image/svg+xml</dc:format><dc:type rdf:resource="http://purl.org/dc/dcmitype/StillImage" /></cc:Work></rdf:RDF></metadata><defs id="defs10" /><sodipodi:namedview pagecolor="#ffffff" bordercolor="#666666" borderopacity="1" objecttolerance="10" gridtolerance="10" guidetolerance="10" inkscape:pageopacity="0" inkscape:pageshadow="2" inkscape:window-width="640" inkscape:window-height="480" id="namedview8" showgrid="false" inkscape:zoom="0.13169643" inkscape:cx="896" inkscape:cy="896" inkscape:window-x="0" inkscape:window-y="25" inkscape:window-maximized="0" inkscape:current-layer="svg2" /><g transform="matrix(1,0,0,-1,121.49153,1270.2373)" id="g4"><path d="m 1511,480 q 0,-5 -1,-7 Q 1446,205 1242,38.5 1038,-128 764,-128 618,-128 481.5,-73 345,-18 238,84 L 109,-45 Q 90,-64 64,-64 38,-64 19,-45 0,-26 0,0 v 448 q 0,26 19,45 19,19 45,19 h 448 q 26,0 45,-19 19,-19 19,-45 0,-26 -19,-45 L 420,266 q 71,-66 161,-102 90,-36 187,-36 134,0 250,65 116,65 186,179 11,17 53,117 8,23 30,23 h 192 q 13,0 22.5,-9.5 9.5,-9.5 9.5,-22.5 z m 25,800 V 832 q 0,-26 -19,-45 -19,-19 -45,-19 h -448 q -26,0 -45,19 -19,19 -19,45 0,26 19,45 l 138,138 Q 969,1152 768,1152 634,1152 518,1087 402,1022 332,908 321,891 279,791 271,768 249,768 H 50 Q 37,768 27.5,777.5 18,787 18,800 v 7 q 65,268 270,434.5 205,166.5 480,166.5 146,0 284,-55.5 138,-55.5 245,-156.5 l 130,129 q 19,19 45,19 26,0 45,-19 19,-19 19,-45 z" id="path6" inkscape:connector-curvature="0" style="fill:currentColor" /></g></svg>';

//So each icon gets its own unique id
let aiId = 0;


/* Finds instances of coordinates and adds the icon html with data-coordinates */
function addHtmlElements() {
    /*
        The gc website is... not well structured semantically
        so we have to treat each coordinate specially.

        We'll create the icon where it's suitable, and tag it with
        the coordinates so we can just loop them all and fetch data
        later without needing to find the coordinates.
     */

    regularCachePageCoordinates();
    reviewerCachePageCoordinates();
    additionalWaypointsCoordinates();
}

/* Additional waypoints */
function additionalWaypointsCoordinates(event) {
    if (typeof event !== "undefined") {
        event.preventDefault();
    }

    let el, td, wpName, raaIcon;
    let wpRows = $('#ctl00_ContentBody_Waypoints tbody tr');
    let iconCheck = $('#wpReload');

    let foundProperRows = 0;
    if (wpRows.length > 0) {
        wpRows.each(function () {
            el = $(this);
            /*
             * Regular interface has 6 sub-td, admin has 8.
             */

            /* 6 td-children, so regular interface */
            if (el.children('td').length === 6) {
                if (typeof (el.attr('ishidden')) !== "undefined" && el.attr('ishidden') === 'false') {
                    /* Physical of final? */
                    wpName = el.children('td').eq(4)[0].lastChild.textContent.trim().slice(1, -1);
                    if (wpName === "Final Location" || wpName === "Physical Stage") {
                        foundProperRows++;
                        /* Ok, a relevant waypoint with visible coordinates */
                        td = el.children('td').eq(5);
                        if (td.length > 0) {
                            raaIcon = createRaaIconElement(td.text());
                            raaIcon.appendTo(td); //Add the raa-icon in the td-element since the coordinates is a textnode
                        }
                    }
                }
            } else if (el.children('td').length >= 8) {
                /* Reviewer interface */
                wpName = el.children('td').eq(5)[0].lastChild.textContent.trim().slice(1, -1);
                if (wpName === "Final Location" || wpName === "Physical Stage") {
                    foundProperRows++;
                    td = el.children('td').eq(6).children('a'); //Not a td, but meh...
                    if (td.length > 0) {
                        raaIcon = createRaaIconElement(td.text());
                        raaIcon.insertAfter(td);
                    }
                }
            }
        });
        //Remove reload icon if it exists, also trigger coordinates check
        if (iconCheck.length > 0 && foundProperRows > 0) {
            iconCheck.remove();
            automaticSearch();
        }
    }

    //If we didn't find any legit rows, add the reload button
    if (foundProperRows === 0) {
        /* Is there a reload icon already? */
        if (iconCheck.length === 0) {
            /* Create the reload icon */
            let reloadIcon = $('<a/>', {
                'class': 'reloadIcon',
                id: 'wpReload',
                href: '#'
            }).click(additionalWaypointsCoordinates);
            reloadIcon.html(reloadSvg);
            reloadIcon.appendTo($('#ctl00_ContentBody_WaypointsInfo'));
        }
    }
}

/* Regular cache page, published or unpublished */
function regularCachePageCoordinates() {
    /* This is for published caches, where we need to place the icon next to the element with coordinates */
    let target = $('#uxLatLonLink');
    if (target.length > 0) {
        let coordinates = $('#uxLatLon').text().trim();
        let raaIcon = createRaaIconElement(coordinates);
        raaIcon.insertAfter(target); //Add the raa-icon after the coordinates-link
    } else {
        //Unpublished as regular user
        target = $('#uxLatLon');
        let coordinates = target.text();
        let cacheCoords = createRaaIconElement(coordinates);
        cacheCoords.appendTo(target); //Add the raa-icon in the td-element since the coordinates is a textnode
    }
}

/* Unpublished caches reviewer interface */
function reviewerCachePageCoordinates() {
    let target = $('#ctl00_ContentBody_CacheDetails_Coords');
    if (target.length > 0) {
        let coordinates = target.text().trim();
        cacheCoords = createRaaIconElement(coordinates);
        cacheCoords.insertAfter(target); //Add the raa-icon after the coordinates-element
    }
}


/**
 * Takes a latLngObj {lat: X.XXX, lng: Y.YYY} and converts into a boundingbox that Raa understands
 * @param latLngObj
 * @returns {[]}
 */
function boundingRectangle(latLngObj) {
    let rect = [];
    //For ~50m, it's 0.0004 for lat and 0.00075 lng
    rect.push(latLngObj.lng - 0.003); //west
    rect.push(latLngObj.lat - 0.0016); //south
    rect.push(latLngObj.lng + 0.003); //east
    rect.push(latLngObj.lat + 0.0016); //north
    return rect;
}

function fetchDataFromRaa(id, coords) {
    let rectString = boundingRectangle(coords).join(' ');
    let url = 'http://kulturarvsdata.se/ksamsok/api?method=search&hitsPerPage=1&query=itemType="Kulturlämning" AND boundingBox=/WGS84 "' + rectString + '"';
    let raaIcon = $('#raa-' + id);
    let ret = GM.xmlHttpRequest({
        method: "GET",
        url: url,
        headers: {
            "Accept": "application/json"
        },

        onload: function (res) {
            let result = JSON.parse(res.responseText);
            raaIcon.removeClass('unchecked');
            if (result.result.totalHits === 0) {
                raaIcon.addClass('clear');
            } else {
                raaIcon.addClass('nearby');
            }
        },
        onerror: function (r) {
            raaIcon.removeClass('unchecked');
            raaIcon.addClass('unknown');
        }
    });
}

//Converts the GC-coordinates into float
function convertCoordinates(coordinates) {
    let regex = /([NS]) ?(\d+)° (\d+)\.(\d+) ([EW]) ?(\d+)° (\d+)\.(\d+)/gm;
    let m;
    while ((m = regex.exec(coordinates)) !== null) {
        // This is necessary to avoid infinite loops with zero-width matches
        if (m.index === regex.lastIndex) {
            regex.lastIndex++;
        }

        let latCalc = parseFloat(m[3] + "." + m[4]) / 60;
        let lat = parseInt(m[2]) + latCalc;
        if (m[1] !== 'N') {
            lat = -1 * lat;
        }
        let lngCalc = parseFloat(m[7] + "." + m[8]) / 60;
        let lng = parseInt(m[6]) + lngCalc;
        if (m[5] !== 'E') {
            lng = -1 * lng;
        }

        return { lat: lat, lng: lng };
    }
    return false;
}

/* Finds all instances of the icon and fetch data from Raä */
function automaticSearch() {
    let allCoords = $('.raaCoordinates.unchecked');
    for (let i = 0; i < allCoords.length; i++) {
        let raa = $(allCoords[i]);
        let coords = convertCoordinates(raa.data('coordinates'));
        if (coords !== false) {
            fetchDataFromRaa(raa.data('id'), coords);
        }
    }
}

/*
 * Easy way to create the actual Raa-icon element.
 * Leave whatever called it to insert it into the DOM, since where will differ.
 */
function createRaaIconElement(coordinates) {
    let raaIcon = $('<a/>', {
        'data-coordinates': coordinates.trim(), //Always trim here instead of relying on what called the function
        'class': 'raaIcon unchecked raaCoordinates',
        'data-id': aiId,
        id: 'raa-' + aiId,
        href: '#'
    }).click(openRaaMap);
    raaIcon.html(raaSvg);
    aiId++;
    return raaIcon;
}

function openRaaMap(e) {
    e.preventDefault();
    let coords = convertCoordinates($(this).data('coordinates'));
    let sweref = convertToSweref99tm(coords);
    let lat = sweref.lat;
    let lng = sweref.lng;
    let mapString = 'https://app.raa.se/open/fornsok/plats-query?n=' + lat + '&e=' + lng;
    window.open(mapString, '_blank');
}

function addStyles() {
    let style = $('<style/>');
    let styles = [];
    styles.push('.raaIcon,.reloadIcon {padding: 0 2px;}');
    styles.push('.raaIcon.unchecked svg, .reloadIcon svg { fill: #000; color: #000;}');
    styles.push('.raaIcon.clear svg { fill: #00aa00;}');
    styles.push('.raaIcon.nearby svg { fill: #aa0000;}');
    styles.push('.raaIcon.unknown svg { fill: #0000aa;}');
    styles.push('.raaIcon svg, .reloadIcon svg { width: 15px; height: 15px; }');
    style.text(styles.join(''));
    $('body').append(style);
}


/* Triggers when page has loaded */
function initRaaLookup() {
    addStyles();
    addHtmlElements();
    automaticSearch();
}

//Taken from http://latlong.mellifica.se/
function convertToSweref99tm(coords) {
    let latitude = coords.lat;
    let longitude = coords.lng;

    let axis = 6378137.0; // GRS 80.
    let flattening = 1.0 / 298.257222101; // GRS 80.
    let central_meridian = 15.00;
    let lat_of_origin = 0.0;
    let scale = 0.9996;
    let false_northing = 0.0;
    let false_easting = 500000.0;
    let x_y = new Array(2);

    let e2 = flattening * (2.0 - flattening);
    let n = flattening / (2.0 - flattening);
    let a_roof = axis / (1.0 + n) * (1.0 + n * n / 4.0 + n * n * n * n / 64.0);
    let A = e2;
    let B = (5.0 * e2 * e2 - e2 * e2 * e2) / 6.0;
    let C = (104.0 * e2 * e2 * e2 - 45.0 * e2 * e2 * e2 * e2) / 120.0;
    let D = (1237.0 * e2 * e2 * e2 * e2) / 1260.0;
    let beta1 = n / 2.0 - 2.0 * n * n / 3.0 + 5.0 * n * n * n / 16.0 + 41.0 * n * n * n * n / 180.0;
    let beta2 = 13.0 * n * n / 48.0 - 3.0 * n * n * n / 5.0 + 557.0 * n * n * n * n / 1440.0;
    let beta3 = 61.0 * n * n * n / 240.0 - 103.0 * n * n * n * n / 140.0;
    let beta4 = 49561.0 * n * n * n * n / 161280.0;

    // Convert.
    let deg_to_rad = Math.PI / 180.0;
    let phi = latitude * deg_to_rad;
    let lambda = longitude * deg_to_rad;
    let lambda_zero = central_meridian * deg_to_rad;

    let phi_star = phi - Math.sin(phi) * Math.cos(phi) * (A +
        B * Math.pow(Math.sin(phi), 2) +
        C * Math.pow(Math.sin(phi), 4) +
        D * Math.pow(Math.sin(phi), 6));
    let delta_lambda = lambda - lambda_zero;
    let xi_prim = Math.atan(Math.tan(phi_star) / Math.cos(delta_lambda));
    let eta_prim = math_atanh(Math.cos(phi_star) * Math.sin(delta_lambda));
    let x = scale * a_roof * (xi_prim +
        beta1 * Math.sin(2.0 * xi_prim) * math_cosh(2.0 * eta_prim) +
        beta2 * Math.sin(4.0 * xi_prim) * math_cosh(4.0 * eta_prim) +
        beta3 * Math.sin(6.0 * xi_prim) * math_cosh(6.0 * eta_prim) +
        beta4 * Math.sin(8.0 * xi_prim) * math_cosh(8.0 * eta_prim)) +
        false_northing;
    let y = scale * a_roof * (eta_prim +
        beta1 * Math.cos(2.0 * xi_prim) * math_sinh(2.0 * eta_prim) +
        beta2 * Math.cos(4.0 * xi_prim) * math_sinh(4.0 * eta_prim) +
        beta3 * Math.cos(6.0 * xi_prim) * math_sinh(6.0 * eta_prim) +
        beta4 * Math.cos(8.0 * xi_prim) * math_sinh(8.0 * eta_prim)) +
        false_easting;
    return { lat: Math.round(x * 1000.0) / 1000.0, lng: Math.round(y * 1000.0) / 1000.0 }
}

// Missing functions in the Math library.
function math_sinh(value) {
    return 0.5 * (Math.exp(value) - Math.exp(-value));
}

function math_cosh(value) {
    return 0.5 * (Math.exp(value) + Math.exp(-value));
}

function math_atanh(value) {
    return 0.5 * Math.log((1.0 + value) / (1.0 - value));
}


$(document).ready(function () {
    initRaaLookup();
});
